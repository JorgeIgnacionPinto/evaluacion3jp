/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.evaluacion3jp.services;

import com.mycompany.evaluacion3jp.dao.TrabajadoresJpaController;
import com.mycompany.evaluacion3jp.dao.exceptions.NonexistentEntityException;
import com.mycompany.evaluacion3jp.entity.Trabajadores;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Fr0nt
 */
@Path("trabajadores")
public class trabajadoresRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTrabajadores() {

        TrabajadoresJpaController dao = new TrabajadoresJpaController();
        List<Trabajadores> equipos = dao.findTrabajadoresEntities();

        return Response.ok(200).entity(equipos).build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearTrabajadores(Trabajadores trabajadores) {
        TrabajadoresJpaController dao = new TrabajadoresJpaController();
        try {
            dao.create(trabajadores);
        } catch (Exception ex) {
            Logger.getLogger(trabajadoresRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(trabajadores).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarTrabajadores(Trabajadores equipo) {
        TrabajadoresJpaController dao = new TrabajadoresJpaController();
        try {
            dao.edit(equipo);
        } catch (Exception ex) {
            Logger.getLogger(trabajadoresRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(equipo).build();
    }
    @DELETE
    @Path("/{ruteliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarEquipos(@PathParam("ruteliminar")String eliminar){
        TrabajadoresJpaController dao = new TrabajadoresJpaController();
        try {
            dao.destroy(eliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(trabajadoresRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("cliente eliminado").build();
    }
    
    @GET
    @Path("/{rutConsultar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarEquipos(@PathParam("rutConsultar")String idConsultar){
        
        TrabajadoresJpaController dao = new TrabajadoresJpaController();
        Trabajadores equipos = dao.findTrabajadores(idConsultar);
        
        return Response.ok(200).entity(equipos).build();
        
    }

}
