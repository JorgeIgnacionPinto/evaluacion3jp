/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.evaluacion3jp.dao;

import com.mycompany.evaluacion3jp.dao.exceptions.NonexistentEntityException;
import com.mycompany.evaluacion3jp.dao.exceptions.PreexistingEntityException;
import com.mycompany.evaluacion3jp.entity.Trabajadores;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Fr0nt
 */
public class TrabajadoresJpaController implements Serializable {

    private String trabajadores;

    public TrabajadoresJpaController() {
    }
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("trabajadores_UP");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Trabajadores equipos) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(equipos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findTrabajadores(equipos.getNombre()) != null) {
                throw new PreexistingEntityException("Trabajadores " + trabajadores + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Trabajadores equipos) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            equipos = em.merge(equipos);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = equipos.getNombre();
                if (findTrabajadores(id) == null) {
                    throw new NonexistentEntityException("The equipos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Trabajadores equipos;
            try {
                equipos = em.getReference(Trabajadores.class, id);
                equipos.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The equipos with id " + id + " no longer exists.", enfe);
            }
            em.remove(equipos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Trabajadores> findTrabajadoresEntities() {
        return findTrabajadoresEntities(true, -1, -1);
    }

    public List<Trabajadores> findTrabajadoresEntities(int maxResults, int firstResult) {
        return findTrabajadoresEntities(false, maxResults, firstResult);
    }

    private List<Trabajadores> findTrabajadoresEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Trabajadores.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Trabajadores findTrabajadores(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Trabajadores.class, id);
        } finally {
            em.close();
        }
    }

    public int getTrabajadoresCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Trabajadores> rt = cq.from(Trabajadores.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }

    private List<Trabajadores> findtrabajadoresEntities(boolean b, int i, int i0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private Object findtrabajadores(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Trabajadores> findtrabajadoresEntities() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
