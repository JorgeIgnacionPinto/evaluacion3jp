<%-- 
    Document   : index
    Created on : 5 jul 2021, 20:04:39
    Author     : Fr0nt
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <<h1>Endpoints Api Trabajadores</h1>
        <h1>Lista de Trabajadores - GET - https://evaluacion3jp.herokuapp.com/api/trabajadores</h1>
        <h1>Listar Trabajadores por Rut - GET - https://evaluacion3jp.herokuapp.com/api/trabajadores{RUT}</h1>
        <h1>Agregar Trabajadores - POST - https://evaluacion3jp.herokuapp.com/api/trabajadores</h1>
        <h1>Actualizar Trabajadores - PUT - https://evaluacion3jp.herokuapp.com/api/trabajadores</h1>
        <h1>Eliminar Trabajadores - DELETE - https://evaluacion3jp.herokuapp.com/api/trabajadores{RUT}</h1>
    </body>
</html>
